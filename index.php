<?php

require_once 'views/top.php';
require_once 'views/nav.php';
require_once 'views/home/masthead.php';
require_once 'views/home/question.php';
require_once 'views/home/sections.php';
require_once 'views/end.php';

?>

<script src="dist/js/home/index.js"></script>