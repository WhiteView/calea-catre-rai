<?php

return [
    [
        'title' => 'Păcatul',
        'img-src' => 'dist/assets/img/home/sin.jpg',
        'text' => 'În epistola sfântului apostol Pavel către Romani, capitolul 3, versetul 23, Dumnezeu spune că toți oamenii sunt păcătoși: <em>„Fiindcă toți au păcătuit și nu ajung la gloria lui Dumnezeu”</em>. A păcătui înseamnă încălcarea poruncilor pe care Dumnezeu le dă în Biblie (să nu mințim, să nu furăm ș.a.).',
    ],
    [
        'title' => 'Consecința păcatului',
        'img-src' => 'dist/assets/img/home/consequence.jpg',
        'text' => 'Mai departe, în capitolul 6, versetul 23, Biblia ne spune: <em>„Fiindcă plata păcatului este <strong>moartea</strong>...”</em><br>Merităm să fim pedepsiți pentru lucrurile rele pe care le facem, iar Biblia spune că acea pedeapsă este iadul (moartea).',
    ],
    [
        'title' => 'Iazul de foc',
        'img-src' => 'dist/assets/img/home/lake-of-fire.jpg',
        'text' => 'În Apocalipsa 21, versetul 8, Dumnezeu ne spune ce fel de oameni vor merge în pedeapsa veșnică: <em>„Dar fricoșii și cei care nu cred și scârboșii și ucigașii și curvarii și vrăjitorii și idolatrii și toți mincinoșii vor avea partea lor în lacul care arde cu foc și pucioasă, care este <strong>a doua moarte</strong>”</em>.',
    ],
    [
        'title' => 'Soluția lui Dumnezeu',
        'img-src' => 'dist/assets/img/home/solution.jpg',
        'text' => 'Chiar dacă merităm moartea a doua, Dumnezeu nu vrea să mergem acolo. El ne iubește și vrea să ne încredem în singurul lui Fiu născut, Isus Cristos, și doar în El.',
        'extra' => [
            'text' => 'Evanghelia după Ioan, capitolul 3, versetul 16 (cel mai cunoscut verset din lume) spune așa: <em>„Fiindcă atât de mult a iubit Dumnezeu lumea, că a dat pe singurul său Fiu născut, ca oricine crede în el, să nu piară, ci să aibă viață veșnică”</em>.',
        ]
    ],
    [
        'title' => 'Cine este Isus?',
        'img-src' => 'dist/assets/img/home/cross.jpg',
        'text' => 'Când vine vorba de persoana lui Isus, Biblia este clară: El este însuși Dumnezeu.<br>Evanghelia după Ioan ne luminează în sensul acesta: „La început era Cuvântul şi Cuvântul era cu Dumnezeu şi Cuvântul <strong>era Dumnezeu</strong>. Şi Cuvântul a fost făcut carne şi a locuit printre noi [...]” (Ioan 1:1, 1:14a).',
        'extra' => [
            'text' => 'De asemena, în vechiul testament se profețește despre Isus ca fiind <strong>Dumnezeul puternic</strong>:<br>
            <em>„Căci un copil ni s-a născut, un fiu ni s-a dat, şi domnia va fi pe umărul lui şi îi va fi pus numele Minunat, Sfătuitor, <strong>Dumnezeul puternic</strong>, Tatăl veşnic, Prinţul Păcii.”</em> (Isaia 9:6)',
        ]
    ],
    [
        'title' => 'Credința este îndeajuns',
        'img-src' => 'dist/assets/img/home/praying.jpg',
        'text' => 'Mântuirea (salvarea de la iad) este un cadou gratuit din partea lui Dumnezeu. Scriptura e clară în Efeseni 2:8-9 unde spune că nu faptele noastre bune ne aduc acest dar.',
        'extra' => [
            'text' => '<em>„Fiindcă prin har sunteţi salvaţi, prin credinţă; şi aceasta nu din voi înşivă, ci este darul lui Dumnezeu; <strong>Nu prin fapte</strong>, ca să nu se fălească nimeni.”</em> (Efeseni 2:8-9)<br>Exemple de fapte bune ar fi mersul la biserică, iubirea de semeni, rugăciuni sau ajutorarea orfanilor, însă oricât de buni am fi, nu vom fi niciodată primiți în cer fără credință sinceră în Cristos.',
        ],
    ],
    [
        'title' => 'Siguranța mântuirii',
        'img-src' => 'dist/assets/img/home/mountain.jpg',
        'text' => 'În Evanghelia după Ioan, Domnul Isus face o afirmație care ne poate oferi siguranța că odată ce ne-am pus încrederea în El, nimeni și nimic nu ne va lua viața veșnică.',
        'extra' => [
            'text' => '<em>„Oile mele aud vocea mea şi eu le cunosc şi ele mă urmează; Şi eu le dau viaţă eternă; şi nicidecum <strong>nu vor pieri niciodată</strong> şi nimeni nu le va smulge din mâna mea.”</em> (Ioan 10:27-28)<br>Chiar dacă noi înșine am vrea să ieșim din mâna Domnului, nu am putea. Deși Dumnezeu ne pedepsește pe pământ pentru păcatele pe care vom continua să le facem, nu ne va trimite niciodată în iad.',
        ],
    ],
    [
        'title' => 'Primirea darului',
        'img-src' => 'dist/assets/img/home/receiving.jpg',
        'text' => 'În Romani 10:13, Dumnezeu ne face următoarea promisiunea: <em>„Fiindcă oricine va chema numele Domnului va fi salvat.”</em>. El te așteaptă să vii la El cu credință și să-L accepți ca Salvator al tău!',
        'extra' => [
            'text' => 'Dumnezeu te iubește și își dorește să fi cu El! Așadar, ce alegi? N-ai vrea să chemi numele Domnului chiar acum pentru a primi darul salvării? Roagă-te o rugăciune ca aceasta din toată inima ta:<br><br>”Doamne Isuse, recunosc că sunt un păcătos. Recunosc că trebuie să plătesc pedeapsa pentru propriile mele păcate, dar nu doresc să merg în Iad când mor.<br>Vreau să fiu cu tine în Rai. Cred că ai murit pe cruce și apoi ai înviat din nou pentrumine. Te rog iartă-mi păcatul și vino în inima mea. Accept darul mântuirii tale. Cred doar în tine și mă încred că tu ca și salvator al meu mă vei lua în Rai când voi muri. Îți mulțumesc Doamne pentru dragostea ta. Amin!”',
        ],
    ],
    [
        'title' => 'Ce urmează?',
        'img-src' => 'dist/assets/img/home/gift.jpg',
        'text' => 'Dacă l-ai primit pe Cristos în inima ta, numele tău este scris în cer în cartea vieții, și nimeni și nimic nu-l mai poate șterge de acolo.',
        'extra' => [
            'text' => 'Totuși, Dumnezeu nu vrea să stăm degeaba după ce am primit viața veșnică. El vrea să creștem spiritual, iar asta se face doar prin citirea Bibliei și cu ajutorul rugăciunii și altor persoane. <a tabindex="0" class="popover-dismiss badge bg-primary" role="button" data-bs-toggle="popover" data-bs-trigger="focus" title="2 Timotei 2:22 (noul testament)" data-bs-content="De asemenea fugi de poftele tinereţii şi urmăreşte dreptatea, credinţa, dragostea creştină, pacea cu cei ce cheamă pe Domnul dintr-o inimă pură.">2 Timotei 2:22</a> ne spune să fugim de poftele tinereții, să fim drepți înaintea lui Dumnezeu și că ar trebui să fim împreună cu alți credincioși.<br><br>Dacă acest site ți-a fost recomandat de cineva, ia legătura cu acea persoană! Dacă nu, nu ezita să contactezi adresa <a href="mailto:contact@caleacatrerai.ro" class="badge bg-info">contact@caleacatrerai.ro</a> cu mărturia ta despre cum Dumnezeu te-a luminat prin cele de mai sus.',
        ],
    ],
];
