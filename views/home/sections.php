<?php

$sections = require 'config/home/sections.php';

?>

<section class="projects-section p-3 bg-light">
    <div class="container px-4 px-lg-5">
        <?php foreach ($sections as $number => $section) : ++$number; ?>
            <!-- <?php echo $section['title']; ?> -->
            <div class="row gx-0 mb-4 mb-lg-5 align-items-center">
                <div class="col-xl-4 col-lg-7">
                    <img class="img-fluid rounded shadow-lg mb-3 mb-lg-0" src="<?php echo $section['img-src'] ?>" alt="<?php echo $section['title']; ?>" />
                </div>
                <div class="col-xl-8 col-lg-5">
                    <div class="featured-text text-center text-lg-left">
                        <h1 class="display-6"><?php echo "{$number}. {$section['title']}"; ?></h1>
                        <p class="h3 text-black-50 mb-0">
                            <?php echo $section['text'] ?>
                        </p>
                    </div>
                </div>
                <?php if (isset($section['extra'])) : ?>
                    <div class="row gx-0 mt-3 align-items-center">
                        <div class="featured-text text-center text-lg-left">
                            <p class="h3 text-black-50 mb-0"><?php echo $section['extra']['text']; ?></p>
                        </div>
                    </div>
                <?php endif; ?>
                <hr class="d-none d-lg-block my-3" />
            </div>
        <?php endforeach; ?>
    </div>
</section>