<!-- Masthead -->
<header class="masthead">
    <div class="
          container
          px-4 px-lg-5
          d-flex
          h-100
          align-items-center
          justify-content-center
        ">

        <div class="d-flex justify-content-center">
            <div class="text-center">
                <h1 class="mx-auto my-0 text-uppercase">Calea către rai</h1>
                <h2 class="text-white-50 mx-auto mt-2 mb-5">
                    Biblia, cartea scrisă de însuși Dumnezeu, ne vorbește despre cum ne vede Creatorul nostru. Mergi mai jos pentru o veste bună...
                </h2>
                <a class="btn btn-info" href="#about">Află</a>
            </div>
        </div>
    </div>
</header>