<section class="p-4 text-center" id="about">
    <div class="container px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="featured-text col-lg-8">
                <h2 class="display-5 mb-3">Ce putem face?</h2>
                <p class="h3 mb-5">
                    Mai jos veți găsi adevărul despre cum ne vede Dumnezeu și ce a făcut El pentru noi, oamenii.
                </p>
            </div>
        </div>

        <video controls preload="auto" width="100%" height="100%" poster="dist/assets/img/home/video-thumbnail.jpg" class="rounded shadow">
            <source src="dist/assets/video/gospel.mp4" type="video/mp4" />
        </video>
    </div>
    <hr class="my-3">
</section>