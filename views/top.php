<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="Calea către rai; Biblia; Scriptura" />
    <title>Calea către rai</title>
    <link rel="icon" type="image/x-icon" href="dist/assets/favicon.ico" />
    <link href="dist/css/styles.css" rel="stylesheet" />
    <script src="dist/js/fontawesome.js"></script>
</head>

<body id="page-top">